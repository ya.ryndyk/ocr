import glob
import re
import subprocess
import cv2
import numpy as np
import os


def preprocess_image(img: np.ndarray) -> np.ndarray:
    # check if img is rgb
    img = np.array(img)
    if len(img.shape) == 3:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ksize = (3, 3)
    image_blurred = cv2.blur(img, ksize)

    kernel = np.ones((1, 1), np.uint8)
    img = cv2.dilate(image_blurred, kernel, iterations=1)
    img = cv2.erode(img, kernel, iterations=1)
    return img


def read_text(path: str) -> str:
    with open(path, 'r') as file:
        lines = file.readlines()
    text = ' '.join(lines)
    text = text.replace('\n', '')
    return text

def remove_time(text):
    return text

def postprocess_text(text):
    text = text.replace('  ', ' ')
    text = text.replace('\f', '')
    return text


def get_text_from_image(image: np.ndarray) -> str:
    p = subprocess.Popen('rm -rf tmp/*', stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    p.wait()

    img_prep = preprocess_image(image)
    cv2.imwrite('tmp/1.jpg', img_prep)

    command = f'tesseract tmp/1.jpg tmp/output_text -l rus --psm 6'  # 1>/dev/null 2>&1
    p1 = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    (output, err) = p1.communicate()
    p1.wait()

    text = read_text('tmp/output_text.txt')
    text = postprocess_text(text)
    return text
