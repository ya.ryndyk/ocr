import cv2
import numpy as np
import os
import requests
import json
from flask import Flask, Response, redirect, url_for, request, session, abort
from flask import request, render_template
from ocr import preprocess_image, get_text_from_image

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def do_ocr():
    file = request.files['image'].read()
    npimg = np.fromstring(file, np.uint8)
    img = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
    text = get_text_from_image(img)
    res = {'result': text}
    return res


if __name__ == '__main__':
    app.run(host='0.0.0.0')